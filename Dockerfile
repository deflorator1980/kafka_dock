FROM openjdk:8-jdk
RUN apt-get update && apt install net-tools -y 
RUN wget http://apache.hippo.nl/kafka/2.2.0/kafka_2.12-2.2.0.tgz
RUN tar -xzf kafka_2.12-2.2.0.tgz
WORKDIR /kafka_2.12-2.2.0
RUN pwd
COPY run.sh /kafka_2.12-2.2.0/
COPY server.properties /kafka_2.12-2.2.0/config/
EXPOSE 9092
EXPOSE 2181
CMD ./run.sh


